# coding=utf-8
from cx_Freeze import setup, Executable

# On appelle la fonction setup
setup(
    name = "decision_comitee",
    version = "1",
    description = "decision_comitee",
    executables = [Executable("decision_comitee.py")],
)