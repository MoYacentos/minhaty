# coding=utf-8

import mysql.connector
import pickle
import requests
import threading
import datetime as dt

import xlsxwriter
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import os
import time
import sys
import calendar
reload(sys)

database_name = ""  # "maroc36"  # bi3_chri facebook_share   maroc36
database_password = ''

usr_pwd = []
# "/home/yacentos/parametres.txt"
# "D:\Token_Facebook\parametres.txt"
print " Hi Every Body !! "
fp = open("D:\minhaty_crawling\parametres.txt")
for i, line in enumerate(fp):
    if   i == 0:
        chromedriver = str(line).replace("\n", '')
    elif i == 1:
        database_name = str(line).replace("\n", '')
    elif i == 2:
        database_password = str(line).replace("\n", '')
    elif i == 4:
        SCROLL_PAUSE_TIME = int(str(line).replace("\n", ''))
    else :
        usr = str(line).split(",")
        print usr
        usr_to_add = (usr[0], usr[1], usr[2], usr[3])
        usr_pwd.append(usr_to_add)
fp.close()

def heure_actuelle():
    now = dt.datetime.utcnow()
    print ' Date recherche : ', now.day, "-", now.month, "-", now.year, "__", now.hour, ":", now.minute, ":", now.second


def connect_db():
    print "  Connect to Mysql db  =>  ", database_name
    return mysql.connector.connect(user='root', password=database_password, host='localhost', database=database_name,
                                   charset='utf8mb4', use_unicode=True)

def parcours_reacts():

    sql = " Select caidat from demandeur group by caidat"
    connection = connect_db()
    cursor = connection.cursor()
    cursor2 = connection.cursor()
    cursor.execute(sql)
    caidats = cursor.fetchall()

    for caidat in caidats :


        first_sql = 'select d.cin , d.nom , d.prenom , d.nombre_frere , dm.Situation_familiale , d.caidat' \
                    ', d.Adresse , dm.Pere , dm.Mere , dm.Sexe , dm.Tel , dm.date_naissance ' \
                    ' from demandeur d , demandeur_more dm where dm.Cin_Demandeur=d.CIN and d.caidat=\''+caidat[0]+"'"

        workbook = xlsxwriter.Workbook('AALs\Resultat_'+caidat[0]+'.xlsx')
        feuil1 = workbook.add_worksheet()

        # ajout des en-tetes
        feuil1.write(0, 0, 'Caidat')
        feuil1.write(0, 1, 'CIN')
        feuil1.write(0, 2, 'Nom')
        feuil1.write(0, 3, 'Prenom')
        feuil1.write(0, 4, 'Adresse')
        feuil1.write(0, 5, 'Salaire Annuel')
        feuil1.write(0, 6, 'Nombre_frere')
        feuil1.write(0, 7, 'Situation Familiale')
        feuil1.write(0, 8, 'Freres_Handicape')
        feuil1.write(0, 9, 'Total')


        #print " sql : ", first_sql, "  => ", heure_actuelle()
        cursor.execute(first_sql)

        students = cursor.fetchall()
        #print " sql : ", first_sql, "  => ", heure_actuelle()
        nombre_row = cursor.rowcount
        print " Nombrre row : ", nombre_row
        count = 0
        nbr_students = 0

        if nombre_row > 0:

            for student in students:

                nbr_students += 1
                sql_salaire = "select sum(Total) as total from parents where  CIN_Demandeur='"+str(student[0])+"' group by CIN_Demandeur "
                #print nbr_students,"  _  ",sql_salaire
                cursor2.execute(sql_salaire)
                salaire_etudiant = cursor2.fetchone()
                nombre_row2 = cursor2.rowcount

                total = 0
                salaire = 0

                point_salaire = 13

                point_handicape = 0

                if (student[4] <> ""):
                    point_handicape = 8

                if nombre_row2>0:

                    salaire = salaire_etudiant[0]
                    if (salaire<4000): point_salaire = 13
                    elif (salaire<9000): point_salaire = 12
                    elif (salaire<18000): point_salaire = 11
                    elif (salaire<36000): point_salaire = 10
                    elif (salaire<54000): point_salaire = 9
                    elif (salaire<72000): point_salaire = 8
                    elif (salaire<90000): point_salaire = 7
                    elif (salaire<108000): point_salaire = 6
                    elif (salaire<126000): point_salaire = 5
                    elif (salaire<144000): point_salaire = 4
                    elif (salaire<162000): point_salaire = 3
                    elif (salaire<180000): point_salaire = 2
                    else : point_salaire = 1

                sql_freres = "select nom , prenom , Handicape,Instruit  from freres where CIN_Demandeur='" + str(student[0])+"' "
                #print nbr_students,"  _  ",sql_freres
                cursor2.execute(sql_freres)
                freres = cursor2.fetchall()
                nbr_freres = 0
                nbr_handicape = 0
                nbr_instruit = 0

                for frere in freres:

                    nbr_freres += 1
                    #print frere[1] ," __ ",  student[2] , frere[2],"  __  ", frere[3],"  __  ",frere[2] == '1'  ,frere[3] == '1'
                    #time.sleep(4)
                    if( frere[1] <> student[2] ):
                        if (frere[2] == '1'):
                            point_handicape += 4
                            nbr_handicape += 1
                        if (frere[3] == '1'):
                            #point_handicape += 4
                            nbr_instruit += 1
                    else:
                        nbr_freres -= 1


                point_frere = 0
                if (nbr_freres == 0):
                    point_frere = 0
                elif (nbr_freres == 1):
                    point_frere = 2
                elif (nbr_freres == 2):
                    point_frere = 4
                elif (nbr_freres == 3):
                    point_frere = 6
                elif (nbr_freres == 4):
                    point_frere = 8
                elif (nbr_freres > 4):
                    point_frere = 10

                total = point_frere + point_handicape + point_salaire

                feuil1.write(nbr_students, 0, student[5] )
                feuil1.write(nbr_students, 1, student[0] )
                feuil1.write(nbr_students, 2, student[1] )
                feuil1.write(nbr_students, 3, student[2] )
                feuil1.write(nbr_students, 4, student[6] )
                feuil1.write(nbr_students, 5, salaire )
                feuil1.write(nbr_students, 6, nbr_freres)
                feuil1.write(nbr_students, 7, student[4] )
                feuil1.write(nbr_students, 8, nbr_handicape)
                feuil1.write(nbr_students, 9, total)
        print student[5] ,"   ",nbr_students
        workbook.close()

if __name__ == "__main__":
    parcours_reacts()