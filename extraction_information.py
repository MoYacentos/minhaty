# coding=utf-8
import os
import time
import sys
import xlsxwriter

from selenium import webdriver

reload(sys)
sys.setdefaultencoding('utf-8')


chromedriver = ""
database_name = ""  # "maroc36"  # bi3_chri facebook_share   maroc36
database_password = ''
is_paramtrable = 0
renouvlement_token = 0
mot_cle = ''
parametre_group = ''

usr_pwd = []
# "/home/yacentos/parametres.txt"
# "D:\Token_Facebook\parametres.txt"
print " Hi Every Body !! "
fp = open("D:\minhaty_crawling\parametres.txt")
for i, line in enumerate(fp):
    if i == 0:
        chromedriver = str(line).replace("\n", '')
    elif i == 1:
        database_name = str(line).replace("\n", '')
    elif i == 2:
        database_password = str(line).replace("\n", '')
    elif i == 4:
        SCROLL_PAUSE_TIME = int(str(line).replace("\n", ''))
    else:
        usr = str(line).split(",")
        print usr
        usr_to_add = (usr[0], usr[1], usr[2], usr[3])
        usr_pwd.append(usr_to_add)
fp.close()

from selenium.webdriver.common.keys import Keys


def extract_info_student(st,nbr_etud,ligne_et,nombre_et):
    infos = st.find_elements_by_css_selector("td[class='rf-dt-c']")
    i = 0
    source = ""
    for info in infos:
        informa = info.get_attribute('innerHTML')
        print " info (", nbr_etud , "_",i, ") is : ", informa
        ligne_et.write_string(nombre_et, i, informa)
        i += 1

    source = st.get_attribute('innerHTML')

    links = str(source).split('href="')
    #print " nbr : " , len(links)
    #print str(links[0])
    #print "---------------------------------------------------------------"
    #print str(links[1])
    #print "---------------------------------------------------------------"
    #print str(links[2])

    links_initial = str(links[1]).split('" id="')
    link_initial = links_initial[0]
    print " link_initial : ", link_initial
    ligne_et.write_string(nombre_et, i, link_initial)
    i += 1

    links_second = str(links[2]).split('" id="')
    links_second = links_second[0]
    print " links_second : ", links_second
    ligne_et.write_string(nombre_et, i, links_second)





def Authentification(driver, usr, pwd):
    driver.get("http://minhaty.mi-app.ma")
    elem = driver.find_element_by_id("loginForm:username")
    elem.send_keys(usr)

    elem = driver.find_element_by_id("loginForm:password")
    elem.send_keys(pwd)

    elem.send_keys(Keys.RETURN)
    time.sleep(10)

def Acces_Webdriver():

    os.environ["webdriver.chrome.driver"] = chromedriver
    chrome_options = webdriver.ChromeOptions()
    prefs = {"profile.default_content_setting_values.notifications": 2}
    chrome_options.add_experimental_option("prefs", prefs)
    driver = webdriver.Chrome(chromedriver, chrome_options=chrome_options)
    return driver

def parcours_etudiant(usr, pwd):

    print "******************  debut  *******************"
    workbook = xlsxwriter.Workbook('Minhati_2018.xlsx')
    feuil1 = workbook.add_worksheet()

    # Add a bold format to use to highlight cells.
    bold = workbook.add_format({
        'bold': 1,
        'border': 1,
        'align': 'center',
        'valign': 'vcenter'})

    # ajout des en-tetes
    feuil1.write(0, 0, 'Caidat')
    feuil1.write(0, 1, 'CIN')
    feuil1.write(0, 2, 'MASSAR')
    feuil1.write(0, 3, 'Nom')
    feuil1.write(0, 4, 'Prenom')
    feuil1.write(0, 5, 'Date_depot')
    feuil1.write(0, 6, 'Adresse')
    feuil1.write(0, 7, 'Nombre_frere')
    feuil1.write(0, 8, 'Genre_Enquette')
    feuil1.write(0, 9, 'Demance_initial')
    feuil1.write(0, 10, 'Demande_rectifie')

    driver = Acces_Webdriver()
    Authentification(driver, usr, pwd)

    elem = driver.find_element_by_id("j_idt153")
    elem.click()
    #driver.get("file:///D:/minhaty_crawling/code_source.html")
    time.sleep(10)


    is_next_page = True
    nombre_etudiant = 0



    while (is_next_page):

        students_table = driver.find_element_by_id("consulterDemandesList")
        first_student = students_table.find_elements_by_css_selector("tr[class='rf-dt-r rf-dt-fst-r']")
        first_student = first_student[0]
        nombre_etudiant += 1
        #ligne_etude = feuil1.row(nombre_etudiant)
        extract_info_student(first_student,nombre_etudiant,feuil1,nombre_etudiant)

        other_students = students_table.find_elements_by_css_selector("tr[class='rf-dt-r']")
        for student in other_students :
            nombre_etudiant += 1
            #ligne_etude = feuil1.row(nombre_etudiant)
            extract_info_student(student,nombre_etudiant,feuil1,nombre_etudiant)

        next_page = driver.find_elements_by_id("nextPage")
        if len(next_page) > 0 :
            is_next_page = True
            next = next_page[0]
            next.click()
            time.sleep(10)
        else:
            is_next_page = False
        #if nombre_etudiant > 10 :
            #is_next_page = False

    workbook.close()

def execution_prog():
    usr_pwd.sort(reverse=True)
    usr = usr_pwd[0]
    parcours_etudiant(usr[0], usr[1])

if __name__ == "__main__":
    execution_prog()
    time.sleep(20)