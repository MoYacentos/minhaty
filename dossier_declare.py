# coding=utf-8
import os
import time

import xlsxwriter
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import xlrd

import sys
reload(sys)
sys.setdefaultencoding('utf-8')


chromedriver = ""
database_name = ""  # "maroc36"  # bi3_chri facebook_share   maroc36
database_password = ''
is_paramtrable = 0
renouvlement_token = 0
mot_cle = ''
parametre_group = ''

usr_pwd = []
# "/home/yacentos/parametres.txt"
# "D:\Token_Facebook\parametres.txt"
print " Hi Every Body !! "
fp = open("D:\minhaty_crawling\parametres.txt")
for i, line in enumerate(fp):
    if   i == 0:
        chromedriver = str(line).replace("\n", '')
    elif i == 1:
        database_name = str(line).replace("\n", '')
    elif i == 2:
        database_password = str(line).replace("\n", '')
    elif i == 4:
        SCROLL_PAUSE_TIME = int(str(line).replace("\n", ''))
    else :
        usr = str(line).split(",")
        print usr
        usr_to_add = (usr[0], usr[1], usr[2], usr[3])
        usr_pwd.append(usr_to_add)
fp.close()




def extract_info_brother(st,nbr_etud,ligne_et,nombre_et,cin):

    ligne_et.write_string(nombre_et, 0, cin)
    infos = st.find_elements_by_css_selector("td[class='rf-dt-c']")
    i = 1
    source = ""
    for info in infos:
        informa = info.get_attribute('innerHTML')
        #print " info (", nbr_etud , "_",i, ") is : ", informa
        ligne_et.write_string(nombre_et, i, informa)
        i += 1


def Authentification(driver, usr, pwd):
    driver.get("http://minhaty.mi-app.ma")
    elem = driver.find_element_by_id("loginForm:username")
    elem.send_keys(usr)

    elem = driver.find_element_by_id("loginForm:password")
    elem.send_keys(pwd)

    elem.send_keys(Keys.RETURN)
    time.sleep(10)

def Acces_Webdriver():

    os.environ["webdriver.chrome.driver"] = chromedriver
    chrome_options = webdriver.ChromeOptions()
    prefs = {"profile.default_content_setting_values.notifications": 2}
    chrome_options.add_experimental_option("prefs", prefs)
    driver = webdriver.Chrome(chromedriver, chrome_options=chrome_options)
    return driver



def parcours_etudiant(usr, pwd):

    print "******************  debut  *******************"
    students_workbook = xlrd.open_workbook('Minhati_2018.xlsx', on_demand = True)
    students_feuil1 = students_workbook.sheet_by_index(0)


    parents_workbook = xlsxwriter.Workbook('Declared_parents_2018.xlsx')
    parents_feuil1   = parents_workbook.add_worksheet()
    parents_feuil1.write(0, 0, 'CIN_Demandeur')
    parents_feuil1.write(0, 1, 'CIN_parent')
    parents_feuil1.write(0, 2, 'Nom')
    parents_feuil1.write(0, 3, 'Prenom')
    parents_feuil1.write(0, 4, 'Qualite')
    parents_feuil1.write(0, 5, 'Tel')
    parents_feuil1.write(0, 6, 'Profession')
    parents_feuil1.write(0, 7, 'Adresse')
    parents_feuil1.write(0, 8, 'Commune')
    parents_feuil1.write(0, 9, 'Salaire_Annuel')
    parents_feuil1.write(0, 10, 'Revenu_Annuel')
    parents_feuil1.write(0, 11, 'Revenu_professionnel_annuel')
    parents_feuil1.write(0, 12, 'Autre')

    driver = Acces_Webdriver()
    Authentification(driver, usr, pwd)

    #elem = driver.find_element_by_id("j_idt153")
    #elem.click()

    is_next_page = True
    total_freres = 1
    total_etudiant = 0
    total_parents = 0

    print " nombre _rows   : " ,students_feuil1.nrows

    while total_etudiant < students_feuil1.nrows -1  :
        try:
            print " ------------------------------------------------------------------------------------ "
            total_etudiant += 1
            link = students_feuil1.cell_value(rowx=total_etudiant, colx=10)  # students_feuil1.read(0, 7)
            lien = "http://minhaty.mi-app.ma"+str(link)
            print "( ",total_etudiant,") link is ", lien
            #driver.get("file:///D:/minhaty_crawling/code_source_error.html")
            driver.get(lien)
            time.sleep(0.9)

            cin_demandeur = students_feuil1.cell_value(rowx=total_etudiant, colx=1) #students_feuil1.read(0, 1)

            tuteurField = driver.find_element_by_id("j_idt124:tuteurField:j_idt215:0")
            if (tuteurField.get_attribute("checked") != None):
                tuteurField = "Pere"
            else:
                tuteurField = driver.find_element_by_id("j_idt124:tuteurField:j_idt215:1")
                if (tuteurField.get_attribute("checked") != None):
                    tuteurField = "Mere"
                else:
                    tuteurField = driver.find_element_by_id("j_idt124:tuteurField:j_idt215:2")
                    if (tuteurField.get_attribute("checked") != None):
                        tuteurField = "Autre"
                    else:
                        tuteurField = "Inexistant"


            # EXTRACTION DES PARENTS
            cnieTuteur = driver.find_elements_by_id("j_idt124:cnieTuteur")
            if (len(cnieTuteur) > 0):
                total_parents += 1
                parents_feuil1.write(total_parents, 0, cin_demandeur)

                cnieTuteur = cnieTuteur[0]
                cnieTuteur = cnieTuteur.find_element_by_css_selector("span[class='value ']")
                cnieTuteur = cnieTuteur.get_attribute('innerHTML')
                if "</span>" in cnieTuteur :
                    cnieTuteur = str(cnieTuteur).split("</span>")
                    cnieTuteur = str(cnieTuteur[0]).split(">")
                    cnieTuteur = str(cnieTuteur[1])
                parents_feuil1.write(total_parents, 1, cnieTuteur)

                nomTuteur = driver.find_element_by_id("j_idt124:nomTuteur")
                nomTuteur = nomTuteur.find_element_by_css_selector("span[class='value ']")
                nomTuteur = nomTuteur.get_attribute('innerHTML')
                parents_feuil1.write(total_parents, 2, nomTuteur)

                prenomTuteur = driver.find_element_by_id("j_idt124:prenomTuteur")
                prenomTuteur = prenomTuteur.find_element_by_css_selector("span[class='value ']")
                prenomTuteur = prenomTuteur.get_attribute('innerHTML')
                parents_feuil1.write(total_parents, 3, prenomTuteur)

                parents_feuil1.write(total_parents, 4, tuteurField)

                telTuteur = driver.find_element_by_id("j_idt124:telTuteur")
                telTuteur = telTuteur.find_element_by_css_selector("span[class='value ']")
                telTuteur = telTuteur.get_attribute('innerHTML')
                parents_feuil1.write(total_parents, 5, telTuteur)

                professionTuteur = driver.find_element_by_id("j_idt124:professionTuteur")
                professionTuteur = professionTuteur.find_element_by_css_selector("span[class='value ']")
                professionTuteur = professionTuteur.get_attribute('innerHTML')
                parents_feuil1.write(total_parents, 6, professionTuteur)

                adresseField = driver.find_element_by_id("j_idt124:adresseField")
                adresseField = adresseField.find_element_by_css_selector("span[class='value ']")
                adresseField = adresseField.get_attribute('innerHTML')
                parents_feuil1.write(total_parents, 7, adresseField)

                communeResidenceField = driver.find_element_by_id("j_idt124:communeResidenceField")
                communeResidenceField = communeResidenceField.find_element_by_css_selector("span[class='value ']")
                communeResidenceField = communeResidenceField.get_attribute('innerHTML')
                parents_feuil1.write(total_parents, 8, communeResidenceField)

                salaireAnnuelNetTuteur = driver.find_element_by_id("j_idt124:salaireAnnuelNetTuteur")
                salaireAnnuelNetTuteur = salaireAnnuelNetTuteur.find_element_by_css_selector("span[class='value ']")
                salaireAnnuelNetTuteur = salaireAnnuelNetTuteur.get_attribute('innerHTML')
                salaireAnnuelNetTuteur = str(salaireAnnuelNetTuteur).split(",")
                salaireAnnuelNetTuteur = salaireAnnuelNetTuteur[0]
                salaireAnnuelNetTuteur = float(salaireAnnuelNetTuteur) + 0
                parents_feuil1.write(total_parents, 9, salaireAnnuelNetTuteur)

                revenueAnnuelImmobilierTuteur = driver.find_element_by_id("j_idt124:revenuAnnuelImmobilierTuteur")
                revenueAnnuelImmobilierTuteur = revenueAnnuelImmobilierTuteur.find_element_by_css_selector("span[class='value ']")
                revenueAnnuelImmobilierTuteur = revenueAnnuelImmobilierTuteur.get_attribute('innerHTML')

                revenueAnnuelImmobilierTuteur = str(revenueAnnuelImmobilierTuteur).split(",")
                revenueAnnuelImmobilierTuteur = revenueAnnuelImmobilierTuteur[0]
                revenueAnnuelImmobilierTuteur = float(revenueAnnuelImmobilierTuteur) + 0

                parents_feuil1.write(total_parents, 10, revenueAnnuelImmobilierTuteur)

                revenueAnnuelProfessionnelTuteur = driver.find_element_by_id("j_idt124:revenuAnnuelProfessionnelTuteur")
                revenueAnnuelProfessionnelTuteur = revenueAnnuelProfessionnelTuteur.find_element_by_css_selector("span[class='value ']")
                revenueAnnuelProfessionnelTuteur = revenueAnnuelProfessionnelTuteur.get_attribute('innerHTML')
                revenueAnnuelProfessionnelTuteur = str(revenueAnnuelProfessionnelTuteur).split(",")
                revenueAnnuelProfessionnelTuteur = revenueAnnuelProfessionnelTuteur[0]
                revenueAnnuelProfessionnelTuteur = float(revenueAnnuelProfessionnelTuteur) + 0
                parents_feuil1.write(total_parents, 11, revenueAnnuelProfessionnelTuteur)

                revenueAnnuelAutreTuteur = driver.find_element_by_id("j_idt124:revenuAnnuelAutreTuteur")
                revenueAnnuelAutreTuteur = revenueAnnuelAutreTuteur.find_element_by_css_selector("span[class='value ']")
                revenueAnnuelAutreTuteur = revenueAnnuelAutreTuteur.get_attribute('innerHTML')
                revenueAnnuelAutreTuteur = str(revenueAnnuelAutreTuteur).split(",")
                revenueAnnuelAutreTuteur = revenueAnnuelAutreTuteur[0]
                revenueAnnuelAutreTuteur = float(revenueAnnuelAutreTuteur) + 0
                parents_feuil1.write(total_parents, 12, revenueAnnuelAutreTuteur)

            cnieMere = driver.find_elements_by_id("j_idt124:cnieMere")
            if (len(cnieMere) > 0) :
                cnieMere = cnieMere [0]
                total_parents += 1
                parents_feuil1.write(total_parents, 0, cin_demandeur)

                #cnieMere = driver.find_element_by_id("j_idt124:cnieMere")
                cnieMere = cnieMere.find_element_by_css_selector("span[class='value ']")
                cnieMere = cnieMere.get_attribute('innerHTML')
                if "</span>" in cnieMere :
                    cnieMere = str(cnieMere).split("</span>")
                    cnieMere = str(cnieMere[0]).split(">")
                    cnieMere = str(cnieMere[1])
                parents_feuil1.write(total_parents, 1, cnieMere)

                nomMere = driver.find_element_by_id("j_idt124:nomMere")
                nomMere = nomMere.find_element_by_css_selector("span[class='value ']")
                nomMere = nomMere.get_attribute('innerHTML')
                parents_feuil1.write(total_parents, 2, nomMere)

                prenomMere = driver.find_element_by_id("j_idt124:prenomMere")
                prenomMere = prenomMere.find_element_by_css_selector("span[class='value ']")
                prenomMere = prenomMere.get_attribute('innerHTML')
                parents_feuil1.write(total_parents, 3, prenomMere)

                parents_feuil1.write(total_parents, 4, "Mere")

                telMere = driver.find_element_by_id("j_idt124:telMere")
                telMere = telMere.find_element_by_css_selector("span[class='value ']")
                telMere = telMere.get_attribute('innerHTML')
                parents_feuil1.write(total_parents, 5, telMere)

                professionMere = driver.find_element_by_id("j_idt124:professionMere")
                professionMere = professionMere.find_element_by_css_selector("span[class='value ']")
                professionMere = professionMere.get_attribute('innerHTML')
                parents_feuil1.write(total_parents, 6, professionMere)

                adresseField = driver.find_element_by_id("j_idt124:adresseField")
                adresseField = adresseField.find_element_by_css_selector("span[class='value ']")
                adresseField = adresseField.get_attribute('innerHTML')
                parents_feuil1.write(total_parents, 7, adresseField)

                communeResidenceField = driver.find_element_by_id("j_idt124:communeResidenceField")
                communeResidenceField = communeResidenceField.find_element_by_css_selector("span[class='value ']")
                communeResidenceField = communeResidenceField.get_attribute('innerHTML')
                parents_feuil1.write(total_parents, 8, communeResidenceField)

                salaireAnnuelNetMere = driver.find_element_by_id("j_idt124:salaireAnnuelNetMere")
                salaireAnnuelNetMere = salaireAnnuelNetMere.find_element_by_css_selector("span[class='value ']")
                salaireAnnuelNetMere = salaireAnnuelNetMere.get_attribute('innerHTML')
                salaireAnnuelNetMere = str(salaireAnnuelNetMere).split(",")
                salaireAnnuelNetMere = salaireAnnuelNetMere[0]
                salaireAnnuelNetMere = float(salaireAnnuelNetMere) + 0
                parents_feuil1.write(total_parents, 9, salaireAnnuelNetMere)

                revenueAnnuelImmobilierMere = driver.find_element_by_id("j_idt124:revenuAnnuelImmobilierMere")
                revenueAnnuelImmobilierMere = revenueAnnuelImmobilierMere.find_element_by_css_selector(
                    "span[class='value ']")
                revenueAnnuelImmobilierMere = revenueAnnuelImmobilierMere.get_attribute('innerHTML')
                revenueAnnuelImmobilierMere = str(revenueAnnuelImmobilierMere).split(",")
                revenueAnnuelImmobilierMere = revenueAnnuelImmobilierMere[0]
                revenueAnnuelImmobilierMere = float(revenueAnnuelImmobilierMere) + 0
                parents_feuil1.write(total_parents, 10, revenueAnnuelImmobilierMere)

                revenueAnnuelProfessionnelMere = driver.find_element_by_id("j_idt124:revenuAnnuelProfessionnelMere")
                revenueAnnuelProfessionnelMere = revenueAnnuelProfessionnelMere.find_element_by_css_selector(
                    "span[class='value ']")
                revenueAnnuelProfessionnelMere = revenueAnnuelProfessionnelMere.get_attribute('innerHTML')
                revenueAnnuelProfessionnelMere = str(revenueAnnuelProfessionnelMere).split(",")
                revenueAnnuelProfessionnelMere = revenueAnnuelProfessionnelMere[0]
                revenueAnnuelProfessionnelMere = float(revenueAnnuelProfessionnelMere) + 0
                parents_feuil1.write(total_parents, 11, revenueAnnuelProfessionnelMere)

                revenueAnnuelAutreMere = driver.find_element_by_id("j_idt124:revenuAnnuelAutreMere")
                revenueAnnuelAutreMere = revenueAnnuelAutreMere.find_element_by_css_selector("span[class='value ']")
                revenueAnnuelAutreMere = revenueAnnuelAutreMere.get_attribute('innerHTML')
                revenueAnnuelAutreMere = str(revenueAnnuelAutreMere).split(",")
                revenueAnnuelAutreMere = revenueAnnuelAutreMere[0]
                revenueAnnuelAutreMere = float(revenueAnnuelAutreMere) + 0
                parents_feuil1.write(total_parents, 12, revenueAnnuelAutreMere)

        except Exception as e:
            total_parents += 1
            parents_feuil1.write(total_parents, 0, cin_demandeur)
            parents_feuil1.write(total_parents, 1, str(e))

            print " ID introuvable :  ", e

    parents_workbook.close()

    students_workbook.release_resources()
    del students_workbook

def execution_prog():
    usr_pwd.sort(reverse=True)
    usr = usr_pwd[0]
    parcours_etudiant(usr[0], usr[1])

if __name__ == "__main__":
    execution_prog()
    time.sleep(20)