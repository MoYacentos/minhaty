# coding=utf-8
import os
import time

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import xlrd

import sys

from openpyxl import load_workbook

reload(sys)
sys.setdefaultencoding('utf-8')

chromedriver = ""
database_name = ""  # "maroc36"  # bi3_chri facebook_share   maroc36
database_password = ''
is_paramtrable = 0
renouvlement_token = 0
mot_cle = ''
parametre_group = ''

usr_pwd = []
# "/home/yacentos/parametres.txt"
# "D:\Token_Facebook\parametres.txt"
print " Hi Every Body !! "
fp = open("D:\minhaty_crawling\parametres.txt")
for i, line in enumerate(fp):
    if i == 0:
        chromedriver = str(line).replace("\n", '')
    elif i == 1:
        database_name = str(line).replace("\n", '')
    elif i == 2:
        database_password = str(line).replace("\n", '')
    elif i == 4:
        SCROLL_PAUSE_TIME = int(str(line).replace("\n", ''))
    else:
        usr = str(line).split(",")
        print usr
        usr_to_add = (usr[0], usr[1], usr[2], usr[3])
        usr_pwd.append(usr_to_add)
fp.close()


def Authentification(driver, usr, pwd):
    driver.get("http://minhaty.mi-app.ma")
    time.sleep(2)
    elem = driver.find_element_by_id("loginForm:username")
    elem.send_keys(usr)

    elem = driver.find_element_by_id("loginForm:password")
    elem.send_keys(pwd)

    elem.send_keys(Keys.RETURN)
    time.sleep(5)


def Acces_Webdriver():
    os.environ["webdriver.chrome.driver"] = chromedriver
    chrome_options = webdriver.ChromeOptions()
    prefs = {"profile.default_content_setting_values.notifications": 2}
    chrome_options.add_experimental_option("prefs", prefs)
    driver = webdriver.Chrome(chromedriver, chrome_options=chrome_options)
    return driver


def extract_info_student(st, nbr_etud, shee,nbr_ra):
    infos = st.find_elements_by_css_selector("td[class='rf-dt-c']")
    i = 0
    cin = infos[0]
    cin_text = cin.get_attribute('innerHTML')

    not_selected = False

    for i in range(1, shee.nrows):
        # print i," cin  ", shee.cell(i,1).value
        if shee.cell(i, 1).value == str(cin_text):
            not_selected = True
            break

    elements_selected = st.find_elements_by_css_selector("table[class='classradioBoursier']")
    print " elements table elements_selected ", len(elements_selected)
    if len(elements_selected) > 0 :
        elements_selec = elements_selected[0]

        if not_selected == True:
            #id = "j_idt126:reunionDemandeList:" + str(nbr_etud) + ":idAvis:1"
            #elements = elements_selec.find_elements_by_id(id)
            elements = elements_selec.find_elements_by_xpath("//input[@value='2']")
            print " elements not selected ", len(elements)
            if (len(elements) > 0):
                elem = elements[nbr_ra]
                elem.click()
        else:
            #id = "j_idt126:reunionDemandeList:" + str(nbr_etud) + ":idAvis:0"
            #elements = elements_selec.find_elements_by_id(id)
            elements = elements_selec.find_elements_by_xpath("//input[@value='1']")
            print " elements selected ", len(elements)
            if (len(elements) > 0):
                elem = elements[nbr_ra]
                elem.click()
        print cin_text, "   ", not_selected, "  ", id
    #time.sleep(2)


def parcours_etudiant(usr, pwd):
    print "******************  debut  *******************"

    wb = xlrd.open_workbook(filename='Liste_par_salaire_apres_aal_copy.xlsx')
    sheet = wb.sheet_by_index(0)

    driver = Acces_Webdriver()
    Authentification(driver, usr, pwd)

    driver.get("http://minhaty.mi-app.ma/MINHATI/ReunionDecisionEdit.seam?interfacePrec=ReunionDecisionList&cid=50104")
    time.sleep(20)

    is_next = True
    nombre_etudiant = 0
    nbr_radio = 0
    try:
        while True:
            students = driver.find_elements_by_css_selector("table[class='rf-dt']")
            print " table ", len(students)

            if (len(students) > 0):
                nbr_radio = 0
                print " nombre etudiant  ==============< ", nombre_etudiant
                students_table = students[1]

                first_student = students_table.find_elements_by_css_selector("tr[class='rf-dt-r rf-dt-fst-r']")
                first_student = first_student[0]
                # ligne_etude = feuil1.row(nombre_etudiant)
                extract_info_student(first_student, nombre_etudiant, sheet,nbr_radio)
                nombre_etudiant += 1
                nbr_radio += 1

                other_students = students_table.find_elements_by_css_selector("tr[class='rf-dt-r']")
                for student in other_students:
                    # ligne_etude = feuil1.row(nombre_etudiant)
                    extract_info_student(student, nombre_etudiant, sheet, nbr_radio)
                    nombre_etudiant += 1
                    nbr_radio += 1

                # driver.find_element_by_css_selector("input[class='j_idt126:update']")

                time.sleep(20)
                # page.click()



    except Exception as e:
        print " ID introuvable :  ", e

    driver.close()


def execution_prog():
    usr_pwd.sort(reverse=True)
    usr = usr_pwd[0]
    parcours_etudiant(usr[0], usr[1])


if __name__ == "__main__":
    execution_prog()
    time.sleep(20)
