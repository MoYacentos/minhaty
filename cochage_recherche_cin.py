# coding=utf-8
import os
import time

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import xlrd

import sys

from openpyxl import load_workbook

reload(sys)
sys.setdefaultencoding('utf-8')

chromedriver = ""
database_name = ""  # "maroc36"  # bi3_chri facebook_share   maroc36
database_password = ''
is_paramtrable = 0
renouvlement_token = 0
mot_cle = ''
parametre_group = ''

usr_pwd = []
# "/home/yacentos/parametres.txt"
# "D:\Token_Facebook\parametres.txt"
print " Hi Every Body !! "
fp = open("D:\minhaty_crawling\parametres.txt")
for i, line in enumerate(fp):
    if i == 0:
        chromedriver = str(line).replace("\n", '')
    elif i == 1:
        database_name = str(line).replace("\n", '')
    elif i == 2:
        database_password = str(line).replace("\n", '')
    elif i == 4:
        SCROLL_PAUSE_TIME = int(str(line).replace("\n", ''))
    else:
        usr = str(line).split(",")
        print usr
        usr_to_add = (usr[0], usr[1], usr[2], usr[3])
        usr_pwd.append(usr_to_add)
fp.close()


def Authentification(driver, usr, pwd):
    driver.get("http://minhaty.mi-app.ma")
    time.sleep(2)
    elem = driver.find_element_by_id("loginForm:username")
    elem.send_keys(usr)

    elem = driver.find_element_by_id("loginForm:password")
    elem.send_keys(pwd)

    elem.send_keys(Keys.RETURN)
    time.sleep(5)


def Acces_Webdriver():
    os.environ["webdriver.chrome.driver"] = chromedriver
    chrome_options = webdriver.ChromeOptions()
    prefs = {"profile.default_content_setting_values.notifications": 2}
    chrome_options.add_experimental_option("prefs", prefs)
    driver = webdriver.Chrome(chromedriver, chrome_options=chrome_options)
    return driver


def extract_info_student(cin_text, nombre_etudiant , drv ):
            infos = drv.find_elements_by_id("j_idt126:reunionDemandeList:j_idt224")
            infos_len = len(infos)

            if infos_len > 0 :
                infos = infos[0]

                infos.send_keys(Keys.CONTROL, "a")
                infos.send_keys(cin_text)
                infos.send_keys(Keys.RETURN)
            time.sleep(4)

            #elements = elements_selec.find_elements_by_id(id)
            elements = drv.find_elements_by_xpath("//input[@value='1']")
           # print " elements selected ", len(elements)
           # if (len(elements) > 0):
                #elem = elements[0]
                #elem.click()
            #print cin_text, "   selected  "
            #time.sleep(6)


def parcours_etudiant(usr, pwd):
        print "******************  debut  *******************"

        wb = xlrd.open_workbook(filename='Liste_par_salaire_apres_aal_copy.xlsx')
        sheet = wb.sheet_by_index(0)

        driver = Acces_Webdriver()
        Authentification(driver, usr, pwd)

        driver.get("http://minhaty.mi-app.ma/MINHATI/ReunionDecisionEdit.seam?interfacePrec=ReunionDecisionList&cid=50104")

        time.sleep(20)

        is_next = True
        nombre_etudiant = 0
        nbr_radio = 0


        for i in range(1, sheet.nrows):
            try:
                # print i," cin  ", shee.cell(i,1).value
                cin_text =  sheet.cell(i, 1).value
                # ligne_etude = feuil1.row(nombre_etudiant)
                extract_info_student(cin_text, nombre_etudiant , driver )
                nombre_etudiant += 1

                # driver.find_element_by_css_selector("input[class='j_idt126:update']")


                # page.click()



            except Exception as e:
                print " ID introuvable :  ", e

        time.sleep(10)
        driver.close()


def execution_prog():
    usr_pwd.sort(reverse=True)
    usr = usr_pwd[0]
    parcours_etudiant(usr[0], usr[1])


if __name__ == "__main__":
    execution_prog()
    time.sleep(20)
