# coding=utf-8
import os
import time
import xlrd
import csv
import xlsxwriter
import xlwt
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import xlrd

import sys
reload(sys)
sys.setdefaultencoding('utf-8')


def parcours_etudiant():

    print "******************  debut  *******************"

    freres_workbook = xlrd.open_workbook('Minhati_2018.xlsx', on_demand=True)
    demandeur_feuil1_rd = freres_workbook.sheet_by_index(0)

    freres_workbook_wr = xlwt.Workbook(encoding="utf-8")
    demandeur_feuil1_wt = freres_workbook_wr.add_sheet("Demandeurs")

    demandeur_feuil1_wt.write(0, 0, 'Caidat')
    demandeur_feuil1_wt.write(0, 1, 'CIN')
    demandeur_feuil1_wt.write(0, 2, 'MASSAR')
    demandeur_feuil1_wt.write(0, 3, 'Nom')
    demandeur_feuil1_wt.write(0, 4, 'Prenom')
    demandeur_feuil1_wt.write(0, 5, 'Date_depot')
    demandeur_feuil1_wt.write(0, 6, 'Adresse')
    demandeur_feuil1_wt.write(0, 7, 'Nombre_frere')
    demandeur_feuil1_wt.write(0, 8, 'Genre_Enquette')
    demandeur_feuil1_wt.write(0, 9, 'Demance_initial')
    demandeur_feuil1_wt.write(0, 10, 'Demande_rectifie')

    total_etudiant = 0

    print " nombre _rows   : ", demandeur_feuil1_rd.nrows
    # try:
    while total_etudiant < demandeur_feuil1_rd.nrows - 1:
        print " ------------------------------------------------------------------------------------ "
        total_etudiant += 1
        caidat = str(demandeur_feuil1_rd.cell_value(rowx=total_etudiant, colx=0))
        print len(caidat), " caidat is ", caidat
        caidat = caidat.split("/")

        if len(caidat)>1 :
            caidat = caidat[1]
        else:
            caidat = str(caidat[0])
        print len(caidat)," caidat is ",caidat
        caidat = caidat.strip()
        demandeur_feuil1_wt.write(total_etudiant, 0, caidat)
        demandeur_feuil1_wt.write(total_etudiant, 1, str(demandeur_feuil1_rd.cell_value(rowx=total_etudiant, colx=1)).strip())
        demandeur_feuil1_wt.write(total_etudiant, 2, str(demandeur_feuil1_rd.cell_value(rowx=total_etudiant, colx=2)).strip())
        demandeur_feuil1_wt.write(total_etudiant, 3, str(demandeur_feuil1_rd.cell_value(rowx=total_etudiant, colx=3)).strip())
        demandeur_feuil1_wt.write(total_etudiant, 4, str(demandeur_feuil1_rd.cell_value(rowx=total_etudiant, colx=4)).strip())
        demandeur_feuil1_wt.write(total_etudiant, 5, str(demandeur_feuil1_rd.cell_value(rowx=total_etudiant, colx=5)).strip())
        demandeur_feuil1_wt.write(total_etudiant, 6, str(demandeur_feuil1_rd.cell_value(rowx=total_etudiant, colx=6)).strip())
        demandeur_feuil1_wt.write(total_etudiant, 7, str(demandeur_feuil1_rd.cell_value(rowx=total_etudiant, colx=7)).strip())
        demandeur_feuil1_wt.write(total_etudiant, 8, str(demandeur_feuil1_rd.cell_value(rowx=total_etudiant, colx=8)).strip())
        demandeur_feuil1_wt.write(total_etudiant, 9, str(demandeur_feuil1_rd.cell_value(rowx=total_etudiant, colx=9)).strip())
        demandeur_feuil1_wt.write(total_etudiant, 10, str(demandeur_feuil1_rd.cell_value(rowx=total_etudiant, colx=10)).strip())

        print "( ", total_etudiant, ") CIN_Demandeur ", str(
            demandeur_feuil1_rd.cell_value(rowx=total_etudiant, colx=0)).strip(), " = ",

    freres_workbook_wr.save("Demandeurs_updated.xls")
    freres_workbook.release_resources()
    del freres_workbook

    with xlrd.open_workbook('Demandeurs_updated.xls') as wb:
        sh = wb.sheet_by_index(0)  # or wb.sheet_by_name('name_of_the_sheet_here')
        with open('Demandeurs_updated.csv', 'wb') as f:  # open('a_file.csv', 'w', newline="") for python 3
            c = csv.writer(f)
            for r in range(sh.nrows):
                c.writerow(sh.row_values(r))


    # PARENTS
    parents_workbook = xlrd.open_workbook('parents_2018.xlsx', on_demand=True)
    parents_feuil1_rd = parents_workbook.sheet_by_index(0)

    parents_workbook_wr = xlwt.Workbook(encoding="utf-8")
    parents_feuil1_wt = parents_workbook_wr.add_sheet("parents")

    parents_feuil1_wt.write(0, 0, 'CIN_Demandeur')
    parents_feuil1_wt.write(0, 1, 'CIN_parent')
    parents_feuil1_wt.write(0, 2, 'Nom')
    parents_feuil1_wt.write(0, 3, 'Prenom')
    parents_feuil1_wt.write(0, 4, 'Qualite')
    parents_feuil1_wt.write(0, 5, 'Tel')
    parents_feuil1_wt.write(0, 6, 'Profession')
    parents_feuil1_wt.write(0, 7, 'Adresse')
    parents_feuil1_wt.write(0, 8, 'Commune')
    parents_feuil1_wt.write(0, 9, 'Salaire_Annuel')
    parents_feuil1_wt.write(0, 10, 'Revenu_Annuel')
    parents_feuil1_wt.write(0, 11, 'Revenu_professionnel_annuel')
    parents_feuil1_wt.write(0, 12, 'Autre')
    parents_feuil1_wt.write(0, 13, 'Total')

    total_etudiant = 0

    print " nombre _rows   : " ,parents_feuil1_rd.nrows
    #try:
    while total_etudiant < parents_feuil1_rd.nrows -1  :
            print " ------------------------------------------------------------------------------------ "
            total_etudiant += 1

            parents_feuil1_wt.write(total_etudiant, 0, str(parents_feuil1_rd.cell_value(rowx=total_etudiant, colx=0)).strip())

            parents_feuil1_wt.write(total_etudiant, 1, str(parents_feuil1_rd.cell_value(rowx=total_etudiant, colx=1)).strip())
            parents_feuil1_wt.write(total_etudiant, 2, str(parents_feuil1_rd.cell_value(rowx=total_etudiant, colx=2)).strip())
            parents_feuil1_wt.write(total_etudiant, 3, str(parents_feuil1_rd.cell_value(rowx=total_etudiant, colx=3)).strip())
            parents_feuil1_wt.write(total_etudiant, 4, str(parents_feuil1_rd.cell_value(rowx=total_etudiant, colx=4)).strip())
            parents_feuil1_wt.write(total_etudiant, 5, str(parents_feuil1_rd.cell_value(rowx=total_etudiant, colx=5)).strip())
            parents_feuil1_wt.write(total_etudiant, 6, str(parents_feuil1_rd.cell_value(rowx=total_etudiant, colx=6)).strip())
            parents_feuil1_wt.write(total_etudiant, 7, str(parents_feuil1_rd.cell_value(rowx=total_etudiant, colx=7)).strip())
            parents_feuil1_wt.write(total_etudiant, 8, str(parents_feuil1_rd.cell_value(rowx=total_etudiant, colx=8)).strip())
            parents_feuil1_wt.write(total_etudiant, 9, str(parents_feuil1_rd.cell_value(rowx=total_etudiant, colx=9)).strip())
            parents_feuil1_wt.write(total_etudiant, 10,str(parents_feuil1_rd.cell_value(rowx=total_etudiant, colx=10)).strip())
            parents_feuil1_wt.write(total_etudiant, 11,str(parents_feuil1_rd.cell_value(rowx=total_etudiant, colx=11)).strip())
            parents_feuil1_wt.write(total_etudiant, 12,str(parents_feuil1_rd.cell_value(rowx=total_etudiant, colx=12)).strip())


            CIN_Demandeur = parents_feuil1_rd.cell_value(rowx=total_etudiant, colx=0)  # students_feuil1.read(0, 7)

            Salaire_Annuel = parents_feuil1_rd.cell_value(rowx=total_etudiant, colx=9)  # students_feuil1.read(0, 7)
            Salaire_Annuel = str(Salaire_Annuel).split(",")
            Salaire_Annuel = Salaire_Annuel[0]
            Salaire_Annuel = float(Salaire_Annuel) + 0

            Revenu_Annuel = parents_feuil1_rd.cell_value(rowx=total_etudiant, colx=10)  # students_feuil1.read(0, 7)
            Revenu_Annuel = str(Revenu_Annuel).split(",")
            Revenu_Annuel = Revenu_Annuel[0]
            Revenu_Annuel = float(Revenu_Annuel) + 0

            Revenu_professionnel_annuel = parents_feuil1_rd.cell_value(rowx=total_etudiant, colx=11)  # students_feuil1.read(0, 7)
            Revenu_professionnel_annuel = str(Revenu_professionnel_annuel).split(",")
            Revenu_professionnel_annuel = Revenu_professionnel_annuel[0]
            Revenu_professionnel_annuel = float(Revenu_professionnel_annuel) + 0

            Autre = parents_feuil1_rd.cell_value(rowx=total_etudiant, colx=12)  # students_feuil1.read(0, 7)
            Autre = str(Autre).split(",")
            Autre = Autre[0]
            Autre = float(Autre) + 0

            somme = Salaire_Annuel + Revenu_Annuel + Revenu_professionnel_annuel + Autre

            print "( ",total_etudiant,") CIN_Demandeur ", CIN_Demandeur,  " = ",somme
            parents_feuil1_wt.write(total_etudiant, 13, somme)

    parents_workbook_wr.save("Parents_updated.xls")
    parents_workbook.release_resources()
    del parents_workbook

    with xlrd.open_workbook('Parents_updated.xls') as wb:
        sh = wb.sheet_by_index(0)  # or wb.sheet_by_name('name_of_the_sheet_here')
        with open('Parents_updated.csv', 'wb') as f:  # open('a_file.csv', 'w', newline="") for python 3
            c = csv.writer(f)
            for r in range(sh.nrows):
                c.writerow(sh.row_values(r))

    freres_workbook = xlrd.open_workbook('freres_2018.xlsx', on_demand=True)
    freres_feuil1_rd = freres_workbook.sheet_by_index(0)

    freres_workbook_wr = xlwt.Workbook(encoding="utf-8")
    freres_feuil1_wt = freres_workbook_wr.add_sheet("freres")

    freres_feuil1_wt.write(0, 0, 'CIN_Demandeur')
    freres_feuil1_wt.write(0, 1, 'Nom')
    freres_feuil1_wt.write(0, 2, 'Prenom')
    freres_feuil1_wt.write(0, 3, 'Date_Naissance')
    freres_feuil1_wt.write(0, 4, 'Handicape')
    freres_feuil1_wt.write(0, 5, 'Instruit')

    total_etudiant = 0

    print " nombre _rows   : " ,freres_feuil1_rd.nrows
    #try:
    while total_etudiant < freres_feuil1_rd.nrows -1  :
            print " ------------------------------------------------------------------------------------ "
            total_etudiant += 1

            freres_feuil1_wt.write(total_etudiant, 0, str(freres_feuil1_rd.cell_value(rowx=total_etudiant, colx=0)).strip())
            freres_feuil1_wt.write(total_etudiant, 1, str(freres_feuil1_rd.cell_value(rowx=total_etudiant, colx=1)).strip())
            freres_feuil1_wt.write(total_etudiant, 2, str(freres_feuil1_rd.cell_value(rowx=total_etudiant, colx=2)).strip())
            freres_feuil1_wt.write(total_etudiant, 3, str(freres_feuil1_rd.cell_value(rowx=total_etudiant, colx=3)).strip())
            freres_feuil1_wt.write(total_etudiant, 4, str(freres_feuil1_rd.cell_value(rowx=total_etudiant, colx=4)).strip())
            freres_feuil1_wt.write(total_etudiant, 5, str(freres_feuil1_rd.cell_value(rowx=total_etudiant, colx=5)).strip())

            print "( ",total_etudiant,") CIN_Demandeur ", str(freres_feuil1_rd.cell_value(rowx=total_etudiant, colx=0)).strip(),  " = ",


    freres_workbook_wr.save("freres_updated.xls")
    freres_workbook.release_resources()
    del freres_workbook

    with xlrd.open_workbook('freres_updated.xls') as wb:
        sh = wb.sheet_by_index(0)  # or wb.sheet_by_name('name_of_the_sheet_here')
        with open('freres_updated.csv', 'wb') as f:  # open('a_file.csv', 'w', newline="") for python 3
            c = csv.writer(f)
            for r in range(sh.nrows):
                c.writerow(sh.row_values(r))

    students_more_info_workbook = xlrd.open_workbook('students_more_info.xlsx', on_demand=True)
    students_more_info_feuil1_rd = students_more_info_workbook.sheet_by_index(0)

    students_more_info_workbook_wr = xlwt.Workbook(encoding="utf-8")
    students_more_info_feuil1_wt = students_more_info_workbook_wr.add_sheet("students_more_info")

    students_more_info_feuil1_wt.write(0, 0, 'Cin_Demandeur')
    students_more_info_feuil1_wt.write(0, 1, 'Date_naissance')
    students_more_info_feuil1_wt.write(0, 2, 'Adresse_electronique')
    students_more_info_feuil1_wt.write(0, 3, 'Tel')
    students_more_info_feuil1_wt.write(0, 4, 'Sexe')
    students_more_info_feuil1_wt.write(0, 5, 'Situation_familiale')
    students_more_info_feuil1_wt.write(0, 6, 'Pere')
    students_more_info_feuil1_wt.write(0, 7, 'Mere')
    students_more_info_feuil1_wt.write(0, 8, 'Tuteur')

    total_etudiant = 0

    print " nombre _rows   : ", students_more_info_feuil1_rd.nrows
    # try:
    while total_etudiant < students_more_info_feuil1_rd.nrows - 1:
        print " ------------------------------------------------------------------------------------ "
        total_etudiant += 1

        students_more_info_feuil1_wt.write(total_etudiant, 0, str(students_more_info_feuil1_rd.cell_value(rowx=total_etudiant, colx=0)).strip())
        students_more_info_feuil1_wt.write(total_etudiant, 1, str(students_more_info_feuil1_rd.cell_value(rowx=total_etudiant, colx=1)).strip())
        students_more_info_feuil1_wt.write(total_etudiant, 2, str(students_more_info_feuil1_rd.cell_value(rowx=total_etudiant, colx=2)).strip())
        students_more_info_feuil1_wt.write(total_etudiant, 3, str(students_more_info_feuil1_rd.cell_value(rowx=total_etudiant, colx=3)).strip())
        students_more_info_feuil1_wt.write(total_etudiant, 4, str(students_more_info_feuil1_rd.cell_value(rowx=total_etudiant, colx=4)).strip())
        situation = str(students_more_info_feuil1_rd.cell_value(rowx=total_etudiant, colx=5))
        situation = situation.replace("<label>",'')
        situation = situation.replace("</label>",'')
        situation = situation.strip()
        students_more_info_feuil1_wt.write(total_etudiant, 5, situation)
        students_more_info_feuil1_wt.write(total_etudiant, 6, str(students_more_info_feuil1_rd.cell_value(rowx=total_etudiant, colx=6)).strip())
        students_more_info_feuil1_wt.write(total_etudiant, 7, str(students_more_info_feuil1_rd.cell_value(rowx=total_etudiant, colx=7)).strip())
        students_more_info_feuil1_wt.write(total_etudiant, 8, str(students_more_info_feuil1_rd.cell_value(rowx=total_etudiant, colx=8)).strip())

        print "( ", total_etudiant, ") CIN_Demandeur ", str(students_more_info_feuil1_rd.cell_value(rowx=total_etudiant, colx=0)).strip()

    students_more_info_workbook_wr.save("students_more_info_updated.xls")
    students_more_info_workbook.release_resources()
    del students_more_info_workbook

    with xlrd.open_workbook('students_more_info_updated.xls') as wb:
        sh = wb.sheet_by_index(0)
        with open('students_more_info_updated.csv', 'wb') as f:
            c = csv.writer(f)
            for r in range(sh.nrows):
                c.writerow(sh.row_values(r))


if __name__ == "__main__":
    parcours_etudiant()
    #time.sleep(20)