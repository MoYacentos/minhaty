# coding=utf-8
import linecache
import mysql.connector
import pickle
import requests
import threading
import datetime as dt

import xlrd
import xlsxwriter
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import os
import time
import sys
import calendar
reload(sys)

database_name = ""  # "maroc36"  # bi3_chri facebook_share   maroc36
database_password = ''
def PrintException():
    exc_type, exc_obj, tb = sys.exc_info()
    f = tb.tb_frame
    lineno = tb.tb_lineno
    filename = f.f_code.co_filename
    linecache.checkcache(filename)
    line = linecache.getline(filename, lineno, f.f_globals)
    print 'EXCEPTION IN ({}, LINE {} "{}"): {}'.format(filename, lineno, line.strip(), exc_obj)

usr_pwd = []
# "/home/yacentos/parametres.txt"
# "D:\Token_Facebook\parametres.txt"
print " Hi Every Body !! "
fp = open("D:\minhaty_crawling\parametres.txt")
for i, line in enumerate(fp):
    if   i == 0:
        chromedriver = str(line).replace("\n", '')
    elif i == 1:
        database_name = str(line).replace("\n", '')
    elif i == 2:
        database_password = str(line).replace("\n", '')
    elif i == 4:
        SCROLL_PAUSE_TIME = int(str(line).replace("\n", ''))
    else :
        usr = str(line).split(",")
        print usr
        usr_to_add = (usr[0], usr[1], usr[2], usr[3])
        usr_pwd.append(usr_to_add)
fp.close()

def heure_actuelle():
    now = dt.datetime.utcnow()
    print ' Date recherche : ', now.day, "-", now.month, "-", now.year, "__", now.hour, ":", now.minute, ":", now.second


def connect_db():
    print "  Connect to Mysql db  =>  ", database_name
    return mysql.connector.connect(user='root', password=database_password, host='localhost', database=database_name,
                                   charset='utf8mb4', use_unicode=True)

def parcours_etudiant(usr, pwd):

    print "******************  debut  *******************"
    students_workbook = xlrd.open_workbook('Declared_parents_2018.xlsx', on_demand = True)
    students_feuil1 = students_workbook.sheet_by_index(0)
    nbr = students_feuil1.nrows

    connexion = connect_db()
    cursor = connexion.cursor(buffered=True)

    total_etudiant = 1


    print " nombre _rows   : " ,students_feuil1.nrows
    try:
        while total_etudiant < nbr  :
            CIN_Demandeur= students_feuil1.cell_value(rowx=total_etudiant, colx=0)
            CIN_parent= students_feuil1.cell_value(rowx=total_etudiant, colx=1)
            Nom= students_feuil1.cell_value(rowx=total_etudiant, colx=2)
            Prenom= students_feuil1.cell_value(rowx=total_etudiant, colx=3)
            Qualite= students_feuil1.cell_value(rowx=total_etudiant, colx=4)
            Tel= students_feuil1.cell_value(rowx=total_etudiant, colx=5)
            Profession= students_feuil1.cell_value(rowx=total_etudiant, colx=6)
            Adresse= students_feuil1.cell_value(rowx=total_etudiant, colx=7)
            Commune= students_feuil1.cell_value(rowx=total_etudiant, colx=8)
            Salaire_Annuel= students_feuil1.cell_value(rowx=total_etudiant, colx=9)
            Revenu_Annuel= students_feuil1.cell_value(rowx=total_etudiant, colx=10)
            Revenu_professionnel_annuel= students_feuil1.cell_value(rowx=total_etudiant, colx=11)
            Autre= students_feuil1.cell_value(rowx=total_etudiant, colx=12)
            total= students_feuil1.cell_value(rowx=total_etudiant, colx=13)
            #print "  Revenu_professionnel_annuel  ",Commune
            sql = " insert into declared_parents values ('"+str(CIN_Demandeur)+"','"+str(CIN_parent)+"','"\
                  +Nom+"','"+Prenom+"','"+Qualite+"','"+\
                  str(Tel)+"','"+Profession+"','"+Adresse+"','"+\
                  Commune+"','"+str(int(Salaire_Annuel))+"','"+str(int(Revenu_Annuel))+"','"+ \
                  str(int(Revenu_professionnel_annuel))+"','"+str(int(Autre))+"','"+str(int(total))+"')"
            print nbr," __ ", total_etudiant, " , ", sql
            cursor.execute(sql)
            connexion.commit()
            total_etudiant += 1



    except Exception as e:
        PrintException()

    students_workbook.release_resources()
    del students_workbook

def execution_prog():
    usr_pwd.sort(reverse=True)
    usr = usr_pwd[0]
    parcours_etudiant(usr[0], usr[1])

if __name__ == "__main__":
    execution_prog()
    time.sleep(20)