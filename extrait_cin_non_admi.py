# coding=utf-8
import linecache
import os
import time

import xlsxwriter
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import xlrd

import sys

from openpyxl import load_workbook

reload(sys)
sys.setdefaultencoding('utf-8')

chromedriver = ""
database_name = ""  # "maroc36"  # bi3_chri facebook_share   maroc36
database_password = ''
is_paramtrable = 0
renouvlement_token = 0
mot_cle = ''
parametre_group = ''

usr_pwd = []
# "/home/yacentos/parametres.txt"
# "D:\Token_Facebook\parametres.txt"
print " Hi Every Body !! "
fp = open("D:\minhaty_crawling\parametres.txt")
for i, line in enumerate(fp):
    if i == 0:
        chromedriver = str(line).replace("\n", '')
    elif i == 1:
        database_name = str(line).replace("\n", '')
    elif i == 2:
        database_password = str(line).replace("\n", '')
    elif i == 4:
        SCROLL_PAUSE_TIME = int(str(line).replace("\n", ''))
    else:
        usr = str(line).split(",")
        print usr
        usr_to_add = (usr[0], usr[1], usr[2], usr[3])
        usr_pwd.append(usr_to_add)
fp.close()


def Authentification(driver, usr, pwd):
    driver.get("http://minhaty.mi-app.ma")
    time.sleep(2)
    elem = driver.find_element_by_id("loginForm:username")
    elem.send_keys(usr)

    elem = driver.find_element_by_id("loginForm:password")
    elem.send_keys(pwd)

    elem.send_keys(Keys.RETURN)
    time.sleep(8)


def Acces_Webdriver():
    os.environ["webdriver.chrome.driver"] = chromedriver
    chrome_options = webdriver.ChromeOptions()
    prefs = {"profile.default_content_setting_values.notifications": 2}
    chrome_options.add_experimental_option("prefs", prefs)
    driver = webdriver.Chrome(chromedriver, chrome_options=chrome_options)
    return driver


def extract_info_student(sht, nombre_etudiant , st,drv,nbr_a ,sht2):
            infos = st.find_elements_by_css_selector("td[class='rf-dt-c']")
            i = 0
            cin = infos[0]
            cin_text = cin.get_attribute('innerHTML')
            #elements = elements_selec.find_elements_by_id(id)

            elements = st.find_elements_by_xpath("//input[@value='2']")
           # print " elements selected ", len(elements)
            if (len(elements) > 0):
                elem = elements[nbr_a]

                #attrs = drv.execute_script(
                #    'var items = {}; for (index = 0; index < arguments[0].attributes.length; ++index) { items[arguments[0].attributes[index].name] = arguments[0].attributes[index].value }; return items;',elem)
                #print(attrs)
                checked =  elem.get_attribute("checked")

                if checked != None:
                    print nbr_a,"  ",cin_text, " chcked 2 ", checked
                    sht.write(nombre_etudiant,0,cin_text)

            elements = st.find_elements_by_xpath("//input[@value='1']")
            # print " elements selected ", len(elements)
            if (len(elements) > 0):
                elem = elements[nbr_a]

                # attrs = drv.execute_script(
                #    'var items = {}; for (index = 0; index < arguments[0].attributes.length; ++index) { items[arguments[0].attributes[index].name] = arguments[0].attributes[index].value }; return items;',elem)
                # print(attrs)
                checked = elem.get_attribute("checked")

                if checked != None:
                    print nbr_a,"  ",cin_text, " chcked 1 ", checked
                    sht2.write(nombre_etudiant, 0, cin_text)

            return  cin_text
            #print cin_text, "   selected  "
            #time.sleep(6)


def parcours_etudiant(usr, pwd):
        print "******************  debut  *******************"

        workbook = xlsxwriter.Workbook('non_admis.xlsx')
        feuil1 = workbook.add_worksheet()

        workbook2 = xlsxwriter.Workbook('admis.xlsx')
        feuil2 = workbook2.add_worksheet()

        driver = Acces_Webdriver()
        Authentification(driver, usr, pwd)

        driver.get("http://minhaty.mi-app.ma/MINHATI/ReunionDecisionEdit.seam?interfacePrec=ReunionDecisionList&cid=50104")

        time.sleep(20)

        is_next = True
        nombre_etudiant = 0
        nbr_radio = 0


        cin = ''

        while cin != 'JH55776':
                    try:
                        students = driver.find_elements_by_css_selector("table[class='rf-dt']")
                        print " table ", len(students)

                        if (len(students) > 0):
                            nbr_radio = 0
                            print " nombre etudiant  ==============< ", nombre_etudiant
                            students_table = students[1]

                            first_student = students_table.find_elements_by_css_selector("tr[class='rf-dt-r rf-dt-fst-r']")
                            first_student = first_student[0]
                            # ligne_etude = feuil1.row(nombre_etudiant)
                            cin = extract_info_student(feuil1, nombre_etudiant ,first_student,driver,nbr_radio,feuil2 )
                            nombre_etudiant += 1
                            nbr_radio += 1

                            other_students = students_table.find_elements_by_css_selector("tr[class='rf-dt-r']")
                            for student in other_students:
                                # ligne_etude = feuil1.row(nombre_etudiant)
                                cin = extract_info_student(feuil1, nombre_etudiant ,student,driver,nbr_radio,feuil2 )
                                nombre_etudiant += 1
                                nbr_radio += 1

                            # driver.find_element_by_css_selector("input[class='j_idt126:update']")
                            print " En pause "
                            time.sleep(5)
                            # page.click()


                    # driver.find_element_by_css_selector("input[class='j_idt126:update']")


                    # page.click()

                    except Exception as e:
                        PrintException()
                        cin = 'JH55776'




        time.sleep(10)
        driver.close()
        workbook.close()
        workbook2.close()

def PrintException():
    exc_type, exc_obj, tb = sys.exc_info()
    f = tb.tb_frame
    lineno = tb.tb_lineno
    filename = f.f_code.co_filename
    linecache.checkcache(filename)
    line = linecache.getline(filename, lineno, f.f_globals)
    print 'EXCEPTION IN ({}, LINE {} "{}"): {}'.format(filename, lineno, line.strip(), exc_obj)

def execution_prog():
    usr_pwd.sort(reverse=True)
    usr = usr_pwd[0]
    parcours_etudiant(usr[0], usr[1])


if __name__ == "__main__":
    execution_prog()
    time.sleep(20)
